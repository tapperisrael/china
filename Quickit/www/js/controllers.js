
angular.module('starter.controllers', [])

.controller('AppCtrl', function($scope, $ionicModal, $timeout,$ionicActionSheet,$state,$localStorage) {

  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //$scope.$on('$ionicView.enter', function(e) {
  //});
  
$scope.$on('$ionicView.enter', function(e) 
{
	if ($localStorage.loggedinuserid)
	{
		$scope.name = $localStorage.loggedinusername;
	}
	else
	{
		$scope.name = '';
	}
	
	if ($localStorage.usertype == 1)
	{
		$scope.showSupplier = true;
	}
	else
	{
		$scope.showSupplier = false;
	}		
});
		
$scope.navigateUrl = function (Path,Num) 
{
	window.location.href = Path+String(Num);
}

$scope.logout = function() { 
	var hideSheet = $ionicActionSheet.show({		
	destructiveText: 'Logout',
	titleText: 'Are you sure you want to logout?',
	cancelText: 'Cancel',
	cancel: function() {
		// add cancel code..
	},
	buttonClicked: function(index) {
		//Called when one of the non-destructive buttons is clicked,
		//with the index of the button that was clicked and the button object.
		//Return true to close the action sheet, or false to keep it opened.
		return true;
	},
	destructiveButtonClicked: function(){
		//Called when the destructive button is clicked.
		//Return true to close the action sheet, or false to keep it opened.
		$localStorage.loggedinuserid = '';
		$localStorage.loggedinuseremail = '';
		$localStorage.loggedinusername = '';
		$localStorage.usertype = '';
		$localStorage.quotename = '';
		$localStorage.quotedate = '';
		$localStorage.custumername = '';
		$localStorage.marketname = '';
		$localStorage.custumerid = '';
		$localStorage.catagoryid = '';
		$localStorage.language = '';
		//console.log($localStorage)
		$state.go('app.main');


				
	}
});
}
})



.controller('MainController', function($scope,$http,$rootScope,$localStorage,$ionicSideMenuDelegate,$ionicPopup,$translate) 
{
$ionicSideMenuDelegate.canDragContent(false);
	
$scope.fields = 
{
	'email':'',
	'password':''
}



if ($localStorage.loggedinuserid) {
	window.location.href = "#/app/welcome/";
}  

	$scope.changeLanguage = function (langKey) {
		$translate.use(langKey);
		$rootScope.defaultLanguage = langKey;
	};
	
	
	$scope.getLanguage = function()
	{
		if ($localStorage.language)
		{
			$scope.selectedLanguage = $localStorage.language;
			$scope.selectedLanguageCap = $scope.selectedLanguage.toUpperCase();			
		}
		else
		{
			$scope.selectedLanguage = $rootScope.defaultLanguage;
			$scope.selectedLanguageCap = $scope.selectedLanguage.toUpperCase();			
		}
	}
	
	$scope.getLanguage();
	
	$rootScope.$watch('defaultLanguage', function () 
	{   
		$scope.getLanguage();

	}, true);


$scope.terms = false;

$scope.checklogin = function() 
{
	if ($scope.fields.email =="")
	{
		$ionicPopup.alert({
		title: "please input your email",
		buttons: [{
			text: 'OK',
			type: 'button-positive',
		  }]
	   });				
	}
	else if ($scope.fields.password =="")
	{
		$ionicPopup.alert({
		title: "please input your password",
		buttons: [{
			text: 'OK',
			type: 'button-positive',
		  }]
	   });				
	}
	else
	{
		$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
		$http.post($rootScope.host+'/check_login.php', $scope.fields)
		.success(function(data, status, headers, config)
		{
		  if (data.response && data.response.status == 0) 
		  {
			$ionicPopup.alert({
			title: "email or password doesn't match please try again",
			buttons: [{
				text: 'OK',
				type: 'button-positive',
			  }]
		   });
   
		  }		
		  if (data.response && data.response.userid) 
		  {
			  $localStorage.loggedinuserid = data.response.userid;
			  $localStorage.loggedinuseremail = data.response.email;
			  $localStorage.loggedinusername = data.response.name;
			  $localStorage.usertype = data.response.usertype;
			  if (data.response.todayquote)
			  {
				  $localStorage.catagoryid = data.response.todayquote;
				  window.location.href = "#/app/addproduct/";
			  }
			  else
			  {
				  window.location.href = "#/app/welcome/";
			  }
				  
				  //window.location.href = "#/app/dashboard/"+userid;
			  }  
		  })
		  .error(function(data, status, headers, config)
		  {
		  });			
	}	
};
})


.controller('RegisterController', function($scope,$http,$rootScope,$stateParams,$localStorage,$state,$ionicSideMenuDelegate,$ionicPopup,$translate) 
{
	$scope.navTitle='<p class="HeaderTitle">Sign Up</p>'
	var myDate = new Date();
	var month = myDate.getMonth() + 1;
	var day = myDate.getDate();

	$ionicSideMenuDelegate.canDragContent(false);

	$scope.getLanguage = function()
	{
		if ($localStorage.language)
		{
			$scope.selectedLanguage = $localStorage.language;
			$scope.selectedLanguageCap = $scope.selectedLanguage.toUpperCase();			
		}
		else
		{
			$scope.selectedLanguage = $rootScope.defaultLanguage;
			$scope.selectedLanguageCap = $scope.selectedLanguage.toUpperCase();			
		}
	}
	
	$scope.getLanguage();
	
	$rootScope.$watch('defaultLanguage', function () 
	{   
		$scope.getLanguage();

	}, true);
	
	
	$scope.changeLanguage = function (langKey) {
		$translate.use(langKey);
		$rootScope.defaultLanguage = langKey;
	};

	if(day<10) {
		day='0'+day
	} 
	if(month<10) {
		month='0'+month
	} 
	var prettyDate =(day +'-'+ month) +'-'+ myDate.getFullYear().toString().substr(2,2);
	
	
if ($localStorage.loggedinuserid) 
{
	$scope.getCatagories  = function()
	{
		$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';


		catagory_params = 
		{	
		'user' : $localStorage.loggedinuserid,
		'custumer' : $rootScope.custumerid
		};
		
		$http.post($rootScope.host+'/catagories.php',catagory_params)
		.success(function(data, status, headers, config)
		{
			$scope.quotes = data.response;	
			$scope.exist = 0;
			for (i = 0; i < data.response.length; i++)
			{
				if ($scope.quotes[i].pubdate == prettyDate)
				{
					$scope.exist = 1;
					$scope.todayquotes  = 1;
					$scope.BackButtonText = 'Keep working';
					$scope.startBtnImage = 'img/main/StartBtn-Keep.png';
					$localStorage.catagoryid = $scope.quotes[i].id;	
				}
			}
			
			if ($scope.exist == 1)
			{
				window.location.href = "#/app/addproduct/";
			}
			else
			{
				window.location.href = "#/app/welcome/";
			}
				
		})
		.error(function(data, status, headers, config)
		{

		});	
	}

	$scope.getCatagories();
}  
	
	   
	
$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';

$scope.register = 
{
	"name" : "",
	"email" : "",
	"phone" : "",
	"areacode" : "",
	"password" : "",
	"repassword" : "",
	"type" : "0"
}

$scope.changeRegisterType = function(num)
{
	$scope.register.type = num;
}

$scope.doRegister = function()
{
var emailRegex = /\S+@\S+\.\S+/;
/*
if ($scope.register.name =="")
{
	alert ("please enter your name")
}
*/
if ($scope.register.email =="")
{
	$ionicPopup.alert({
	title: "please input your email address",
	buttons: [{
		text: 'OK',
		type: 'button-positive',
	  }]
   });	
}
else if (emailRegex.test($scope.register.email) == false)
{
	$ionicPopup.alert({
	title: "email not valid please correct",
	buttons: [{
		text: 'OK',
		type: 'button-positive',
	  }]
   });				
	
		$scope.register.email = '';
}
else if ($scope.register.password =="")
{
	$ionicPopup.alert({
	title: "please input a password",
	buttons: [{
		text: 'OK',
		type: 'button-positive',
	  }]
   });	
   
}

else
{
   register_params = 
	{	
		'name' : $scope.register.name,
		'email' : $scope.register.email,
		'password' : $scope.register.password,
		'areacode' : $scope.register.areacode,
		'phone' : $scope.register.phone,
		'type' : $scope.register.type
	};
	//console.log(register_params)
	$http.post($rootScope.host+'/register.php', register_params)
	.success(function(data, status, headers, config)
		{

			if (data.response && data.response.status == 0) 
			{
				$ionicPopup.alert({
				title: "username with this email already exists, please try again or login",
				buttons: [{
					text: 'OK',
					type: 'button-positive',
				  }]
			   });	
   
				$scope.register.email = '';
			}		
			if (data.response && data.response.userid) 
			{
				$scope.register.email = '';
				$scope.register.password = '';
				$scope.register.repassword = '';
				$scope.register.phone = '';
				$scope.register.areacode = '';
				$localStorage.loggedinuserid = data.response.userid;
				$localStorage.loggedinuseremail = data.response.email;
				$localStorage.loggedinusername = data.response.name;
				$localStorage.usertype = $scope.register.type;
				if ($scope.register.type == 0)
				{
					window.location.href = "#/app/welcome/";
				}
				else
				{
					window.location.href = "#/app/supplier/";
				}
				
				//window.location.href = "#/app/dashboard/"+userid;
			}
			
		})
		.error(function(data, status, headers, config)
		{

		});			
}		
}

$scope.LoginBtn = function()
{
	$state.go('app.main');
}
		
})


.controller('Products', function($scope,$http,$rootScope,$stateParams,$localStorage,$ionicPopup) 
{
	 $scope.$on('$ionicView.enter', function(e) 
	 {

	$scope.imagePath = $rootScope.host;
	$scope.showexcel = false;
	$scope.selectstate = 1;
	
    $http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
	
	$scope.showExcelBtn = function()
	{
		$scope.showexcel = true;
		$scope.selectAll();
		
	}
	$scope.selectAll = function()
	{	
		if ($scope.selectstate == 1)
		{
			for (i = 0; i < $scope.products.length; i++)
			{
				$scope.products[i].excel = true;	
			}
			$scope.selectstate = 0;
		}
		else if ($scope.selectstate == 0)
		{
			for (i = 0; i < $scope.products.length; i++)
			{
				$scope.products[i].excel = false;	
			}	
				$scope.selectstate = 1;			
		}			
	}
	
	$scope.sendCsvBtn = function()
	{
	 $scope.ExcelArray = new Array();
	 
	 for (i = 0; i < $scope.products.length; i++)
	 {
		 if ($scope.products[i].excel == true)
		 {
			 $scope.ExcelArray.push($scope.products[i]);
		 }
	 }
	 $scope.ExcelArray = $scope.products;
	// 
	 console.log('excelarray')
	 console.log($scope.ExcelArray);
	 if ($scope.ExcelArray.length == 0)
	 {
			$ionicPopup.alert({
			title: "you must select products to export to csv",
			buttons: [{
				text: 'OK',
				type: 'button-positive',
			  }]
		   });	
		   
	 }
	 else
	 {

	   var confirmPopup = $ionicPopup.confirm({
		 title: 'CSV of the products will be sent to '+$localStorage.loggedinuseremail+'?',
		 template: ''
	   });

	   confirmPopup.then(function(res) {
		if(res) 
		{
			send_params = 
			{	
			'user' : $localStorage.loggedinuserid,
			'custumer' : $localStorage.catagoryid,
			'products' : $scope.ExcelArray
			};
		
		$http.post($rootScope.host+'/products_array_export.php',send_params)
	
		// $http.get($rootScope.host+'/products_export.php?user='+$localStorage.loggedinuserid+'&catagory='+$localStorage.catagoryid)
		.success(function(data, status, headers, config)
			{
				$ionicPopup.alert({
				title: $scope.ExcelArray.length+ " products have been sent as excel to your email.",
				buttons: [{
					text: 'OK',
					type: 'button-positive',
				  }]
				});	
	   
			})
			.error(function(data, status, headers, config)
			{

			});				

		} 

	   });
		 
	 }
	 
	}
	
$scope.getProducts = function()
{
		products_data = 
	{	
		'user' : $localStorage.loggedinuserid,
		'catindex' : $localStorage.catagoryid
	};

	$http.post($rootScope.host+'/products.php', products_data)
	.success(function(data, status, headers, config)
		{
			$scope.products = data.response;
			
			for (i = 0; i < $scope.products.length; i++)
			{
				$scope.products[i].excel = false;
			}
			//console.log($scope.products)
		})
		.error(function(data, status, headers, config)
		{

		});			
}
		
	
$scope.getProducts();

$scope.CheckBoxClicked = function()
{
	//console.log($scope.products)
}
	
			
$scope.deletePrd = function(index,title) 
{ 
		delete_params = 
	{	
		'id' : index,
		'userid' : $localStorage.loggedinuserid
	};
	
   var confirmPopup = $ionicPopup.confirm({
	 title: 'confirm deletion of product '+title+'?',
	 template: ''
   });

   confirmPopup.then(function(res) {
	if(res) 
	{
	  $http.post($rootScope.host+'/del_product.php',delete_params)
	.success(function(data, status, headers, config)
		{
		$scope.getProducts();
		window.location.href = "#/app/products/";
		})
		.error(function(data, status, headers, config)
		{

		});					

	} 

   });

   
   
//confirmbox = confirm('confirm deletion of product '+title+'?');


}

$scope.SelectProduct = function(id)
{
 window.location.href = "#/app/addproduct/"+id;
}
 
$scope.productsCsvBtn  = function()
{
 $scope.ExcelArray = new Array();
 
 for (i = 0; i < $scope.products.length; i++)
 {
	 if ($scope.products[i].excel == true)
	 {
		 $scope.ExcelArray.push($scope.products[i]);
	 }
 }
 
 $scope.ExcelArray = $scope.products;
 
// 
 console.log('excelarray')
 console.log($scope.ExcelArray);
 if ($scope.ExcelArray.length == 0)
 {
		$ionicPopup.alert({
		title: "you must select products to export to csv",
		buttons: [{
			text: 'OK',
			type: 'button-positive',
		  }]
	   });	
	   
 }
 else
 {

   var confirmPopup = $ionicPopup.confirm({
	 title: 'CSV of the products will be sent to '+$localStorage.loggedinuseremail+'?',
	 template: ''
   });

   confirmPopup.then(function(res) {
	if(res) 
	{
		send_params = 
		{	
		'user' : $localStorage.loggedinuserid,
		'custumer' : $localStorage.catagoryid,
		'products' : $scope.ExcelArray
		};
	
	$http.post($rootScope.host+'/products_array_export.php',send_params)

	// $http.get($rootScope.host+'/products_export.php?user='+$localStorage.loggedinuserid+'&catagory='+$localStorage.catagoryid)
	.success(function(data, status, headers, config)
		{
			$ionicPopup.alert({
			title: $scope.ExcelArray.length+ " products have been sent as excel to your email.",
			buttons: [{
				text: 'OK',
				type: 'button-positive',
			  }]
		   });	
   
		})
		.error(function(data, status, headers, config)
		{

		});			

	} 

   });
	 
 }	
}
 
	$scope.editPrd = function(index) 
	{ 
		window.location.href = "#/app/manageprd/";
	}
 
	$scope.addProduct = function () 
	 { 
		window.location.href = "#/app/manageprd/"+$stateParams.id+"/0";
	 }
 
	$scope.newproductBtn = function()
	{
		window.location.href = "#/app/addproduct/-1";
	}
	 
 });
})


.controller('welcomeCtrl', function($scope,$http,$rootScope,$stateParams,$localStorage,$state,$translate,$ionicSideMenuDelegate) 
{
	$scope.navTitle='<p class="HeaderTitle">Quick In</p>'
	//$scope.$on('$ionicView.enter', function(e) {
		
	$rootScope.isSettings  = true;
	
	$scope.fields = 
	{
		"quotename": "New 1",
		"marketname" : ""
	};

	if ($localStorage.loggedinuserid == "")
	{
		$state.go('app.main');
	}

	$scope.getLanguage = function()
	{
		if ($localStorage.language)
		{
			$scope.selectedLanguage = $localStorage.language;
			$scope.selectedLanguageCap = $scope.selectedLanguage.toUpperCase();			
		}
		else
		{
			$scope.selectedLanguage = $rootScope.defaultLanguage;
			$scope.selectedLanguageCap = $scope.selectedLanguage.toUpperCase();			
		}
	}
	
	$scope.getLanguage();

	$rootScope.$watch('defaultLanguage', function () 
	{   
		$scope.getLanguage();

	}, true);	
	
	$scope.changeLanguage = function (langKey) {
		$translate.use(langKey);
		$rootScope.defaultLanguage = langKey;
	};
	


	
	$scope.toggleMenu = function()
	{
		$ionicSideMenuDelegate.toggleLeft();
	}
	  
	  
	//alert ($rootScope.marketId)
	$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
	$scope.isSettings = $rootScope.isSettings;
	$scope.startBtnImage = 'img/start.png';
	$scope.todayquotes = 0;
	//var myDate = new Date();
	//var month = myDate.getMonth() + 1;
	//var prettyDate =(myDate.getDate() +'-'+ month) +'-'+ myDate.getFullYear().toString().substr(2,2);
	var myDate = new Date();
	$scope.month = myDate.getMonth() + 1;
	$scope.day = myDate.getDate();
	$scope.year = myDate.getFullYear().toString().substr(2,2);
	
	
	if($scope.day<10) {
		$scope.day='0'+$scope.day
	} 
	if($scope.month<10) {
		$scope.month='0'+$scope.month
	} 
	var prettyDate =($scope.day +'-'+ $scope.month) +'-'+ myDate.getFullYear().toString().substr(2,2);	

	
	$scope.getCatagories  = function()
	{

		catagory_params = 
		{	
		'user' : $localStorage.loggedinuserid,
		'custumer' : $rootScope.custumerid
		};
		
		$http.post($rootScope.host+'/catagories.php',catagory_params)
		.success(function(data, status, headers, config)
		{
			$scope.quotes = data.response;	
			//$scope.exist = 0;
			for (i = 0; i < data.response.length; i++)
			{
				if ($scope.quotes[i].pubdate == prettyDate)
				{
					$scope.todayquotes  = 1;
					$scope.BackButtonText = 'Keep working';
					$scope.startBtnImage = 'img/start.png';
					$localStorage.catagoryid = $scope.quotes[i].id;
					//$scope.exist = 1;
					
				}
			}

				
		})
		.error(function(data, status, headers, config)
		{

		});	
	}

	$scope.getCatagories();


	$scope.quotedate = prettyDate;
	
	
	$scope.openSettings = function()
	{
		if($scope.isSettings == true)
			$scope.isSettings = false;
		else
		$scope.isSettings = true;
	}
	

			
	if ($localStorage.quotename) 
	{
		$scope.fields.quotename = $localStorage.quotename;
	}
	else 
	{
		$scope.fields.quotename = $scope.fields.quotename;
	}
	
	$scope.setMarket = function()
	{
		if ($localStorage.marketname)
		{
			$scope.fields.marketname = $localStorage.marketname;
		}
		else
		{
			$scope.fields.marketname = $rootScope.defaultMarket;
		}		
	}

		$scope.setMarket();
	
		$rootScope.$watch('defaultMarket', function () 
		{   
			$scope.setMarket();
		}, true);		

		$scope.setCustumer = function()
		{
			if ($localStorage.custumerid)
			{
				$rootScope.defaultCustumerIndex = $localStorage.custumerid  
				$rootScope.defaultCustumer =  $localStorage.custumername;	
				$scope.custumername = $rootScope.defaultCustumer;
			}
			else
			{
				$localStorage.custumerid = $rootScope.defaultCustumerIndex;
				$localStorage.custumername = $rootScope.defaultCustumer;	
				$scope.custumername = $localStorage.custumername;
			}			
		}
		
		$scope.setCustumer();	


		
		$rootScope.$watch('defaultCustumerIndex', function () 
		{   
			$scope.setCustumer();
		}, true);	

		
	$scope.selectMarket = function() 
	{
		$state.go('app.markets');
	}
		
	$scope.startBtn = function() 
	{
	
		$scope.newquotename = $scope.quotedate+' '+$scope.fields.quotename;

		catagory_params = 
		{	
			'user' : $localStorage.loggedinuserid,
			'title' : $scope.newquotename,
			'customer' : $rootScope.custumerid,
			'marketid' : $rootScope.marketId,
			'active_toggles' : 2
		};
		
		console.log("NewQuote")
		console.log(catagory_params)
		console.log($scope.newquotename);
		//alert ($scope.fields.quotename)
		//return;
        $http.post($rootScope.host+'/add_catagory.php', catagory_params)
		.success(function(data, status, headers, config)
		{
			console.log($scope.todayquotes + " : " );
			console.log(data)
			//$scope.catagoryid = $localStorage;
			if ($scope.todayquotes =="1")
			{
					$localStorage.catagoryid = $localStorage.catagoryid;
			}
			else
			{
					$localStorage.catagoryid = data.response.insertid;
			}
			
			$localStorage.catagoryid = data.response.insertid;
			console.log($localStorage.catagoryid + " : " + data.response.insertid);	
			
			$localStorage.quotename = $scope.fields.quotename;
			$localStorage.quotedate  = $scope.quotedate;
			window.location.href = "#/app/addproduct/";
		})
		.error(function(data, status, headers, config)
		{

		});	
	}	
	
//});	
})

.controller('MarketsCtrl', function($scope,$http,$rootScope,$localStorage,$state,$ionicPlatform,$timeout) 
{
	$scope.navTitle='<p class="HeaderTitle">{{"markets_page" | translate}}</p>'
	
	$scope.$on('$ionicView.enter', function(e) {
	$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
	
	$scope.choice = 
	{
		"group" : $rootScope.marketId
	}
	
	if ($rootScope.marketId == -1)
	$scope.yiwuClicked = 1;
	else
	$scope.yiwuClicked = 0;


	$scope.choice.group = -1;

		markets_params = 
		{	
		'user' : $localStorage.loggedinuserid,
		//'customer' : $localStorage.custumerid
		};
	
	$http.post($rootScope.host+'/get_markets.php', markets_params)
	.success(function(data, status, headers, config)
		{
			$scope.markets = data.response;
			
			for (i = 0; i < $scope.markets.length; i++)
			{
				if ($scope.markets[i].id == $rootScope.marketId)
				{
					$scope.markets[i].isClicked = 1;
				}
				else
				{
					$scope.markets[i].isClicked = 0;
				}
				
			}
	
		})
		.error(function(data, status, headers, config)
		{

		});	
			

	$scope.saveMarket = function() 
	{ 
	
	if (!$scope.choice.group)
	$scope.choice.group = -1;

	
	//alert ($scope.choice.group);
	
			save_market_params = 
		{	
			'choice' : $scope.choice.group,
			'user' : $localStorage.loggedinuserid
		};
			
		$rootScope.marketId = $scope.choice.group;
		
		$http.post($rootScope.host+'/save_market.php', save_market_params)
		.success(function(data, status, headers, config)
		{
			
			 //$timeout(function() {
				if ($scope.choice.group == -1)
				{
					$rootScope.defaultMarket = 'YIWU FUTIEN';
				}
				else
				{
					$rootScope.defaultMarket = data.response.marketname;
				}
				$state.go('app.welcome');
			//}, 300);

			

		})
		.error(function(data, status, headers, config)
		{

		});				
	}

	
	$scope.openOther = function() 
	{ 
		$state.go('app.other');
	}
	
	
	$scope.changeChoice = function(index,id)
	{
		$scope.yiwuClicked = 0;


		for (i = 0; i < $scope.markets.length; i++)
		{
			$scope.markets[i].isClicked = 0;
		}	

		
		if (index == -1)
		{
			if ($scope.yiwuClicked == 0)
			{
				$scope.yiwuClicked = 1;
			}
			else
			{
				$scope.yiwuClicked = 0;
			}
		}

		else if ($scope.markets[index].isClicked == 1)
		{
			$scope.markets[index].isClicked = 0;
		}
		else
		{
			$scope.markets[index].isClicked = 1;
		}
		
		$scope.choice.group = id;
		
		//alert ($scope.markets[index].marketname);
		
		
		
	}

	$ionicPlatform.registerBackButtonAction(function (event) 
	{
		window.location.href = "#/app/welcome/";
    }
	,100);

	
	});
})

.controller('OtherCtrl', function($scope,$http,$rootScope,$localStorage,$state,$ionicPopup) 
{

	$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';

	$scope.fields = 
	{
		"marketname" : "",
		"address" : ""
	}
	$scope.saveOther = function() 
	{

		if ($scope.fields.marketname == "")
		{
			$ionicPopup.alert({
			title: "please fill in a market name",
			buttons: [{
				text: 'OK',
				type: 'button-positive',
			  }]
		   });				
		}
		else if ($scope.fields.address =="")
		{
			$ionicPopup.alert({
			title: "please fill in a market address",
			buttons: [{
				text: 'OK',
				type: 'button-positive',
			  }]
		   });				
		}
		else
		{
			other_params = 
			{	
			'user' : $localStorage.loggedinuserid,
			'marketname' : $scope.fields.marketname,
			'address' : $scope.fields.address
			//'customer' : $localStorage.custumerid
			};
		
			$http.post($rootScope.host+'/add_other_market.php', other_params)
			.success(function(data, status, headers, config)
				{
					$scope.fields.marketname = '';
					$scope.fields.address = '';
					$state.go('app.markets')
				})
				.error(function(data, status, headers, config)
				{
				});			
		}	
	}
})

.controller('addproductCtrl', function($scope,$http,$cordovaCamera,$rootScope,$stateParams,$localStorage,$ionicModal,$ionicActionSheet,$timeout,$ionicSlideBoxDelegate,$cordovaBarcodeScanner,$ionicPlatform,$ionicSideMenuDelegate,$ionicPopup,$translate) 
{
	$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
	$ionicSideMenuDelegate.canDragContent(false);
	$scope.showexcel = false;
	$scope.selectstate = 1;
	$scope.TotalBooth = 0;
	$scope.cProduct = '-';
	$scope.BoothText = '';
	$scope.buttonText = 'Save';
	$scope.blankbooth = '';
	$scope.validateFields = 0;
	$scope.duplicate = 0;
	$scope.screenHeight = screen.height-100;
	$scope.quotename = $localStorage.quotename;
	$scope.quotedate = $localStorage.quotedate;
	
	//if want to edite again change in HTML params.editable now its isEdit
	$scope.isEdit = 0;
	
	$scope.getLanguage = function()
	{
		if ($localStorage.language)
		{
			$scope.selectedLanguage = $localStorage.language;
			$scope.selectedLanguageCap = $scope.selectedLanguage.toUpperCase();			
		}
		else
		{
			$scope.selectedLanguage = $rootScope.defaultLanguage;
			$scope.selectedLanguageCap = $scope.selectedLanguage.toUpperCase();			
		}
	}
	
	$scope.getLanguage();

	$rootScope.$watch('defaultLanguage', function () 
	{   
		$scope.getLanguage();

	}, true);	
	
	//alert ($scope.selectedLanguage)
	//alert ($scope.selectedLanguageCap)
	
	if($scope.quotedate)
	{
		$scope.DayArray = $scope.quotedate.split("-");
		$scope.Day = $scope.DayArray[0];
		$scope.Month = $scope.DayArray[1];
		$scope.Year = $scope.DayArray[2];
	}
	
	$scope.params = 
	{
		"editable" : false,
		"currentpage" : -1,
		"currentproduct" : 0,
		"productsLength" : "0"
	}
	

	$scope.EditText = 'duplicate';
	$scope.nextbtnClass = 'button button-icon icon ion-ios-download-outline';
	//console.log($scope.currentpage);
	$scope.showsubmit = false;
	$scope.imagePath = $rootScope.host;
	$scope.TotalProducts =  0;
	

	 $scope.catagoryid = $localStorage.catagoryid;
	 console.log("Category")
	 console.log($scope.catagoryid)
	 $scope.DeafultUrl = 'uploads/camera.png';
	 $scope.firstImage = $scope.DeafultUrl;
	 $scope.secondImage = $scope.DeafultUrl;
	 $scope.thirdImage = $scope.DeafultUrl;
	 $scope.Custom_Field1 = '';
	 $scope.Custom_Field2 = '';
	 $scope.Custom_Field3 = '';

	$scope.changeLanguage = function (langKey) {
		$translate.use(langKey);
		$rootScope.defaultLanguage = langKey;
		
	};

	
	$scope.CameraOption = function(index) 
	{
	   if($scope.thirdImage == $scope.DeafultUrl) 
	   { 
		$scope.takePicture(index,1);
		//$scope.CameraOption();
	   }
	   else
	   {		
			$ionicPopup.alert({
			title: "you cant upload more then 3 images for one product",
			buttons: [{
				text: 'OK',
				type: 'button-positive',
			  }]
		   });	
	   }
	};
	
	// destinationType : Camera.DestinationType.DATA_URL,  CAMERA
	$scope.takePicture = function(index,type) {
		 var options ;
		 
		if(index == 1 )
		{
			options = { 
			quality : 75, 
			destinationType : Camera.DestinationType.FILE_URI, 
			sourceType : Camera.PictureSourceType.CAMERA, 
			allowEdit : true,
			encodingType: Camera.EncodingType.JPEG,
			targetWidth: 600,
			targetHeight: 600,
			correctOrientation: true,
			popoverOptions: CameraPopoverOptions,
			saveToPhotoAlbum: false
			};
		}
		else
		{
			options = { 
			quality : 75, 
			destinationType : Camera.DestinationType.FILE_URI, 
			sourceType : Camera.PictureSourceType.PHOTOLIBRARY, 
			allowEdit : true,
			encodingType: Camera.EncodingType.JPEG,
			targetWidth: 600,
			targetHeight: 600,
			correctOrientation: true,
			popoverOptions: CameraPopoverOptions,
			saveToPhotoAlbum: false
			};
		}
	 
        $cordovaCamera.getPicture(options).then(function(imageData) 
		{
			//if(index == 1 )
			//$scope.imgURI = "data:image/jpeg;base64," + imageData;
			//else
			$scope.imgURI = imageData
			var myImg = $scope.imgURI;
			var options = new FileUploadOptions();
			options.mimeType = 'jpeg';
			options.fileKey = "fileUPP";
			options.fileName = $scope.imgURI.substr($scope.imgURI.lastIndexOf('/') + 1);
			//alert(options.fileName)
			var params = {};
			//params.user_token = localStorage.getItem('auth_token');
			//params.user_email = localStorage.getItem('email');
			options.params = params;
			var ft = new FileTransfer();
			ft.upload($scope.imgURI, encodeURI($rootScope.host+'/UploadImg.php'), $scope.onUploadSuccess, $scope.onUploadFail, options);
    	});
		
		$scope.onUploadSuccess = function(data)
		{
			//data = JSON.stringify(data);
			//alert (data);
			//alert(data.response)
			 $timeout(function() {
				if (data.response) {
					if (type == 1)
					{
						if($scope.firstImage == $scope.DeafultUrl)
						$scope.firstImage = data.response;
						else if($scope.secondImage == $scope.DeafultUrl)
						$scope.secondImage = data.response;
						else if($scope.thirdImage == $scope.DeafultUrl)
						$scope.thirdImage = data.response;						
					}
					else
					{
						$scope.serverCardImage = $rootScope.host+data.response;
						$scope.cardImage = data.response;
						
					}
	
				}
			}, 300);
		}
		
		$scope.onUploadFail = function(data)
		{
			//alert("onUploadFail : " + data)
		}
    }
	
	
	//$scope.$on('$ionicView.enter', function(e) {
	
	$scope.checkToggles = function() 
	{ 
		toggles_params = 
		{
			'user' : $localStorage.loggedinuserid,
			'id' : $scope.catagoryid,
			//'quote' : $scope.catagoryid,
		}
		
        $http.post($rootScope.host+'/toggles.php', toggles_params).success(function(data, status, headers, config)
		{			
			$scope.picture_show = (data.response.picture =="1" ? true : false);
			$scope.itemno_show = (data.response.itemno =="1" ? true : false);
			$scope.price_show = (data.response.price =="1" ? true : false);
			$scope.qty_show = (data.response.qty =="1" ? true : false);
			$scope.cbm_show = (data.response.cbm =="1" ? true : false);
			$scope.remarks_show = (data.response.remarks =="1" ? true : false);
			$scope.weight_show = (data.response.weight =="1" ? true : false);
			$scope.supno_show = (data.response.supno =="1" ? true : false);
			$scope.desc_show = (data.response.description =="1" ? true : false);
			$scope.size_show = (data.response.size =="1" ? true : false);
			$scope.material_show = (data.response.material =="1" ? true : false);
			$scope.stock_show = (data.response.stock =="1" ? true : false);
			$scope.custom_show = (data.response.custom_toggle =="1" ? true : false);
			$scope.Custom_Field1 = data.response.custom_field1;
			$scope.custom_show2 = (data.response.custom_toggle2 =="1" ? true : false);
			$scope.Custom_Field2 = data.response.custom_field2;	
			$scope.custom_show3 = (data.response.custom_toggle3 =="1" ? true : false);
			$scope.Custom_Field3 = data.response.custom_field3;					
		}).error(function(data, status, headers, config)
		{

		});	
	}
			
	//})	
	
	$scope.checkToggles();
	
	$scope.emptyfields = function() { 
	$scope.fields = {
		"booth" : "",
		"price" : "",
		"qty" : "",
		"cbm" : "",
		"itemnumber" : "",
		"remarks" : "",
		"weight" : "",
		"supno" : "",
		"desc" : "",
		"size" : "",
		"material" : "",
		"stock" : "",
		"custom" : "",
		"custom2" : "",
		"custom3" : ""	
	}
	}
	
	$scope.emptyfields();

	$scope.validateFunction = function()
	{
		$scope.validateFields = 0;
		if ($scope.fields.price != "")
			$scope.validateFields = 1;
		if ($scope.fields.qty != "")
			$scope.validateFields = 1;
		if ($scope.fields.cbm != "")
			$scope.validateFields = 1;
		if ($scope.fields.itemnumber != "")
			$scope.validateFields = 1;
		if ($scope.fields.remarks != "")
			$scope.validateFields = 1;
		if ($scope.fields.weight != "")
			$scope.validateFields = 1;
		if ($scope.fields.supno != "")
			$scope.validateFields = 1;
		if ($scope.fields.desc != "")
			$scope.validateFields = 1;
		if ($scope.fields.size != "")
			$scope.validateFields = 1;
		if ($scope.fields.material != "")
			$scope.validateFields = 1;
		if ($scope.fields.stock != "")
			$scope.validateFields = 1;
		if ($scope.fields.custom != "")
			$scope.validateFields = 1;
		if ($scope.fields.custom2 != "")
			$scope.validateFields = 1;
		if ($scope.fields.custom3 != "")
			$scope.validateFields = 1;

		return $scope.validateFields;

	}
	
	$scope.changeSupplier = function()
	{
		//alert ($scope.productid);
		//alert ( $scope.catagoryid);
		//alert ($scope.fields.booth);
		if ($scope.catagoryid)
		{
			getdata = 
			{	
				'user' : $localStorage.loggedinuserid,
				'catindex' : $scope.catagoryid,
				'booth' : $scope.fields.booth
			};
			
			$http.post($rootScope.host+'/check_namecard.php', getdata)
			.success(function(data, status, headers, config)
			{
				
				if (data[0].cardimage)
				{
					$scope.cardImage = data[0].cardimage;
				}
				else
				{
					$scope.cardImage = "";
				}

			})
			.error(function(data, status, headers, config)
			{

			});				
		}


		
	}
	
	$scope.saveProductBtn = function(isDuplicate) 
	{ 
	if ($scope.fields.booth)
	{
		//alert ($scope.fields.booth)

		if ($scope.productid) {
			$scope.updateurl = 'update_product.php';
		}
		else {
			$scope.updateurl = 'add_new_product.php';
		}
		
	
		product_params = 
		{
			'user' : $localStorage.loggedinuserid,
			'catindex' : $scope.catagoryid,
			'id' : $scope.productid,
			'booth' : $scope.fields.booth,
			'cardimage' : $scope.cardImage,
			'title' : $scope.productname,
			'price' : $scope.fields.price,
			'qty' : $scope.fields.qty,
			'cbm' : $scope.fields.cbm,
			'itemnumber' : $scope.fields.itemnumber,
			'remarks' : $scope.fields.remarks,
			'weight' : $scope.fields.weight,
			'supno' : $scope.fields.supno,
			'desc' : $scope.fields.desc,
			'size' : $scope.fields.size,
			'material' : $scope.fields.material,
			'stock' : $scope.fields.stock,
			'custom' : $scope.fields.custom,
			'custom2' : $scope.fields.custom2,
			'custom3' : $scope.fields.custom3,
			'image1' : $scope.firstImage,
			'image2' : $scope.secondImage,
			'image3' : $scope.thirdImage
		};	
		
		
        $http.post($rootScope.host+'/'+$scope.updateurl, product_params)
		.success(function(data, status, headers, config)
			{
			//$scope.productname = '';
			if (!$scope.productid && isDuplicate == 0) { 
			$scope.fields.price = '';
			$scope.fields.qty = '';
			$scope.fields.cbm = '';
			$scope.fields.itemnumber = '';
			$scope.fields.remarks = '';
			$scope.fields.weight = '';
			$scope.fields.supno = '';
			$scope.fields.desc = '';
			$scope.fields.size = '';
			$scope.fields.material = '';
			$scope.fields.stock = '';
			$scope.fields.custom = '';
			$scope.fields.custom2 = '';
			$scope.fields.custom3 = '';
			//$scope.cProduct = $scope.TotalProducts;
			}
			$scope.getProducts();
			$scope.EditText = 'edit';
			//window.location.href = "#/app/addproduct/0";
			})
			.error(function(data, status, headers, config)
			{

			});	
	
	}
	
	else 
	{
		$ionicPopup.alert({
		title: "you must select a booth.",
		buttons: [{
			text: 'OK',
			type: 'button-positive',
		  }]
	   });	
		   
	}
	
	}
		//$scope.$on('$ionicView.enter', function(e) {

	$scope.getProducts = function(setinfo) 
	{	
		$scope.imagePath = $rootScope.host;	
		
		products_data = 
		{	
			'user' : $localStorage.loggedinuserid,
			'catindex' : $scope.catagoryid
		};
		
		$http.post($rootScope.host+'/products.php', products_data)
		.success(function(data, status, headers, config)
		{
			console.log('GetProductResponse : ' , data);
			if (data.response)
			{
				$scope.TotalProducts = data.response.length;
				$scope.params.currentproduct = $scope.TotalProducts-$scope.params.currentpage;
				
				$scope.products = data.response;
				$scope.params.productsLength = data.response.length;
			}	
			else 
			{
				$scope.buttonText = 'Save';
				$scope.nextbtnClass = 'button button-icon icon ion-ios-download-outline';
			}
			
			if (setinfo == 1) 
			$scope.setInfo();
			
			$scope.checkplace();
			
			
			
			if (!$scope.fields.booth)
			$scope.blankbooth = 'supplier information';
			else 
			$scope.blankbooth = '';

		})
		.error(function(data, status, headers, config)
		{

		});	
		

	}

		$scope.getProducts();
		

		$scope.setInfo = function(id) 
		{ 
				//alert (id);
			
				if (id) 
				{
					$scope.params.currentpage = id;
				}
				//else
				//{
				//	$scope.params.currentpage = id;
				//}
				
				$scope.buttonText = 'Save/New';
		
				$scope.productid = $scope.products[$scope.params.currentpage].id;
				$scope.fields.booth = $scope.products[$scope.params.currentpage].booth;
				
				
				$scope.fields.price = parseInt($scope.products[$scope.params.currentpage].price);
				$scope.fields.qty = parseInt($scope.products[$scope.params.currentpage].qty);
				$scope.fields.cbm = $scope.products[$scope.params.currentpage].cbm;
				$scope.fields.itemnumber = $scope.products[$scope.params.currentpage].itemnumber;
				$scope.fields.remarks = $scope.products[$scope.params.currentpage].remarks;
				$scope.fields.weight = $scope.products[$scope.params.currentpage].weight;
				$scope.fields.supno = $scope.products[$scope.params.currentpage].supno;
				$scope.fields.desc = $scope.products[$scope.params.currentpage].desc;
				$scope.fields.size = $scope.products[$scope.params.currentpage].size;
				$scope.fields.material = $scope.products[$scope.params.currentpage].material;
				$scope.fields.stock = $scope.products[$scope.params.currentpage].stock;
				$scope.fields.custom = $scope.products[$scope.params.currentpage].custom;
				$scope.fields.custom2 = $scope.products[$scope.params.currentpage].custom2;
				$scope.fields.custom3 = $scope.products[$scope.params.currentpage].custom3;
				$scope.firstImage = $scope.products[$scope.params.currentpage].image1;
				$scope.secondImage = $scope.products[$scope.params.currentpage].image2;
				$scope.thirdImage = $scope.products[$scope.params.currentpage].image3;
				$scope.TotalBooth = $scope.products[$scope.params.currentpage].num_boothes;
				

				//alert ($scope.TotalBooth.length);
				//console.log("set info: "+ $scope.productid)
				//console.log($scope.fields);
		}
	
		$scope.checkplace = function() 
		{
			//	console.log($scope.currentpage + ': ' + $scope.products.length)
				//alert ($scope.params.currentpage);
				if ($scope.params.currentpage == -1 ) {
					$scope.cProduct = $scope.params.currentproduct;
					$scope.buttonText = 'Save';
					$scope.nextbtnClass = 'button button-icon icon ion-plus-round';
					//$scope.editable = false;
					$scope.params.editable = false;
					
					if($scope.products)
					$scope.fields.booth = $scope.products[0].booth;
					$scope.EditText = 'duplicate';	
					$scope.duplicate = 1;
					//$scope.buttonText = 'Save';
					//$scope.nextbtnClass = 'button button-icon icon ion-ios-download-outline';
					//$scope.cProduct = '-';
					//$scope.editable = false;
									
				}
				else {
					$scope.buttonText = 'Next';
					$scope.nextbtnClass = 'button button-icon icon ion-skip-forward';
					$scope.cProduct = $scope.params.currentproduct;
					//$scope.editable = true;
					$scope.params.editable = true;
					$scope.EditText = 'edit';
				}

			}
		
			$scope.prevItemBtn = function() 
			{
				//alert ('validate: '+ $scope.validateFunction());
				//alert ($scope.params.currentpage);
				//alert ($scope.params.productsLength)
				//$scope.validateFunction() > 0
				
				if($scope.params.currentpage < 0 && $scope.params.productsLength > 0 && $scope.validateFunction() == 1)
				{
				/*
					ConfirmNewProduct = confirm("you open new product - do you want to save it ? ");

						if(ConfirmNewProduct && $scope.validateFunction() > 0)
						{
							$scope.saveProductBtn(0);
						}
				*/				
					   var confirmPopup = $ionicPopup.confirm({
						 title: "you opened new product - do you want to save it ? ",
						 template: ''
					   });

					   confirmPopup.then(function(res) {
						if(res) 
						{
							if ($scope.validateFunction() > 0)
							{
								$scope.saveProductBtn(0);
							}

						} 

					   });
				}
				else if ($scope.validateFunction() != 0)
				{
					$scope.saveProductBtn(0);
				}
				
				//alert("prevItemBtn : " + $scope.params.currentpage )
				if ($scope.params.currentpage < $scope.params.productsLength-1) 
				{ 
					//alert ($scope.currentproduct);
					$scope.params.currentpage++;
					$scope.params.currentproduct--;
					//alert ($scope.currentproduct);
					console.log("prevPage1 : " + $scope.params.currentpage )
					$scope.setInfo();
				//	$scope.saveItem();
				}	
	
				$scope.checkplace()
			}

		

		$scope.nextItemBtn = function() 
		{ 
			if (!$scope.fields.booth)
			{
				$ionicPopup.alert({
				title: "you must select a booth.",
				buttons: [{
					text: 'OK',
					type: 'button-positive',
				  }]
			   });	
				return;
			}
			if ($scope.validateFunction() == 0)
			{
				$ionicPopup.alert({
				title: "please fill in at least one field",
				buttons: [{
					text: 'OK',
					type: 'button-positive',
				  }]
			   });					
				return;
			}
			else 
			{
				$scope.saveProductBtn(0);
			}
			
			//new
			$scope.params.currentpage = 0;
			//alert ($scope.productid)
			if ($scope.params.currentpage > 0) 
			{ 
				$scope.params.currentpage--;
				$scope.params.currentproduct++;
				//console.log("prevPage1 : " + $scope.currentpage )
				$scope.setInfo();
			//	$scope.saveItem();
			}
			else 
			{
				$scope.params.currentpage = -1;
				$scope.params.currentproduct = $scope.TotalProducts+1;
				//console.log('test')
				$scope.productid = '';
				//console.log('end')
				$scope.emptyfields();
				$scope.buttonText = 'Item';
				$scope.firstImage = $scope.DeafultUrl;
				$scope.secondImage = $scope.DeafultUrl;
				$scope.thirdImage = $scope.DeafultUrl;
			}
			
			$scope.checkplace();
			
		}	
		
	
		$scope.editProductBtn = function()
		{
			//if ($scope.duplicate == 1)
			//{
				
				console.log("Editible : " )
				//alert ($scope.params.editable);
				if (!$scope.params.editable)
				{
					if (!$scope.fields.booth)
					{
						$ionicPopup.alert({
						title: "you must select a booth.",
						buttons: [{
							text: 'OK',
							type: 'button-positive',
						  }]
					   });	
					}
					else if ($scope.validateFunction() == 0)
					{
						$ionicPopup.alert({
						title: "please fill in at least one field",
						buttons: [{
							text: 'OK',
							type: 'button-positive',
						  }]
					   });							
						return;
					}
					else 
					{
						$scope.saveProductBtn(1);
					}
					
				}
			
			
				$scope.EditText = 'duplicate';
				$scope.params.editable = false;	
		}
		
		
	//	alert ('state1: '+ $stateParams.productIndex);
		if ($stateParams.productIndex)
	{
	//	alert ('state2: '+ $stateParams.productIndex);
		if ($stateParams.productIndex == -1)
		{
			//alert (123);
			$scope.params.currentpage = -1;
			$scope.params.editable = false;
		}
		else 
		{
		//$scope.setInfo($stateParams.catagoryid);
		$scope.params.currentpage = $stateParams.productIndex;
		$scope.getProducts(1);
		$scope.params.editable = false;			
		}
	}
	
	if ($scope.params.currentpage == -1)
	{
		if ($rootScope.savedFields)
		{
			console.log($rootScope.savedFields);
			$scope.fields = $rootScope.savedFields;
			$rootScope.savedFields = '';		
		}

	}
	

		//products modal.		
		  $ionicModal.fromTemplateUrl('templates/products_modal.html', {
			scope: $scope
		  }).then(function(modal) {
			$scope.modal = modal;
		  });


		   
		  
		  $scope.closeModal = function() 
		  {
			$scope.modal.hide();
		  };

		  $scope.showmodal = function() 
		  {
			$scope.modal.show();
			//fix : $scope.getProducts(1);
			$scope.getProducts();
		  };



		$scope.myProductsBtn = function() 
		{
		//	window.location.href = "#/app/products/";
		$scope.showmodal();
		}
	
		
		$scope.newproductBtn = function()
		{
			$scope.closeModal();
			$scope.params.currentpage = -1;
			$scope.params.editable = false;
			$stateParams.productIndex = -1;
			//window.location.href = "#/app/addproduct/-1";
		}
		
		$scope.deletePrd = function(id,title)  
		{
			
	   var confirmPopup = $ionicPopup.confirm({
		 title: 'confirm deletion of product '+title+'?',
		 template: ''
	   });

	   confirmPopup.then(function(res) {
		if(res) 
		{
			del_parms = 
			{	
				'id' : id,
				'userid' : $localStorage.loggedinuserid
			};
				
				$http.post($rootScope.host+'/del_product.php', del_parms)
			.success(function(data, status, headers, config)
				{
					$scope.getProducts(1);
				})
				.error(function(data, status, headers, config)
				{

				});			
		} 

	   });

		
	}	
	
	$scope.editPrd = function(id) 
	{
		
	//	alert (id);
		if (id == 0) 
		{
			$scope.params.currentpage = 0;
		}
		$scope.setInfo(id);
		$scope.modal.hide();
		//window.location.href = "#/app/prevproduct/"+id+"/1/0";
	}
	
	
	$scope.productsCsv = function() 
	{ 
		$scope.showexcel = true;
		$scope.selectAll();
	}
 
	$scope.selectAll = function()
	{	
		if ($scope.selectstate == 1)
		{
			for (i = 0; i < $scope.products.length; i++)
			{
				$scope.products[i].excel = true;	
			}
			$scope.selectstate = 0;
		}
		else if ($scope.selectstate == 0)
		{
			for (i = 0; i < $scope.products.length; i++)
			{
				$scope.products[i].excel = false;	
			}	
				$scope.selectstate = 1;			
		}			
	}

	$scope.sendCsvBtn = function()
	{
	 $scope.ExcelArray = new Array();
	 
	 for (i = 0; i < $scope.products.length; i++)
	 {
		 if ($scope.products[i].excel == true)
		 {
			 $scope.ExcelArray.push($scope.products[i]);
		 }
	 }
	
	$scope.ExcelArray = $scope.products;	
	// 
	 console.log('excelarray')
	 console.log($scope.ExcelArray);
	 if ($scope.ExcelArray.length == 0)
	 {
			$ionicPopup.alert({
			title: "you must select products to export to csv",
			buttons: [{
				text: 'OK',
				type: 'button-positive',
			  }]
		   });	
		   
	 }
	 else
	 {
		 
	   var confirmPopup = $ionicPopup.confirm({
		 title: 'CSV of the products will be sent to '+$localStorage.loggedinuseremail+'?',
		 template: ''
	   });

	   confirmPopup.then(function(res) {
		if(res) 
		{
			send_params = 
			{	
			'user' : $localStorage.loggedinuserid,
			'custumer' : $localStorage.catagoryid,
			'products' : $scope.ExcelArray
			};
		
		$http.post($rootScope.host+'/products_array_export.php',send_params)
	
		// $http.get($rootScope.host+'/products_export.php?user='+$localStorage.loggedinuserid+'&catagory='+$localStorage.catagoryid)
		.success(function(data, status, headers, config)
			{
				$ionicPopup.alert({
				title: $scope.ExcelArray.length+ " products have been sent as excel to your email.",
				buttons: [{
					text: 'OK',
					type: 'button-positive',
				  }]
			   });	
			})
			.error(function(data, status, headers, config)
			{

			});		

		} 

	   });
		 
	 }
	 
	}

	
	$scope.selectBooth = function(BoothNumber) 
	{ 
		$scope.supplier_image = $scope.DeafultUrl; //$scope.DeafultUrl;
		$scope.supplier_image2 = $scope.DeafultUrl; //$scope.DeafultUrl;
	
			$ionicModal.fromTemplateUrl('templates/suppliers_manage.html', {
			scope: $scope
		  }).then(function(suppliersModal) {
			$scope.suppliersModal = suppliersModal;
			$scope.suppliersModal.show();
		  });			
	
	
		 $scope.fileSelectSupplier = function() 
		{ 
		//$('#fileupload_supplier').click();
		
			   // Show the action sheet
		var hideSheet = $ionicActionSheet.show({
		 buttons: [
		   { text: 'Open Image Gallery' },
		   { text: 'Open Camera' },
		   { text: 'Back'}
		 ],
		 cancelText: 'Cancel',
		 cssClass: 'social-actionsheet',
		 cancel: function() {
			  return true;
			},
		 buttonClicked: function(index) {
			 if(index<2)
			 	$scope.takeSupplierPicture(index);
			 
		   return true;
		 }});
			 // For example's sake, hide the sheet after two seconds
		   $timeout(function() {
			 hideSheet();
		   }, 2000);		
	 }

	$scope.takeSupplierPicture = function(index,picturenumber) {
		 var options ;
		if(index == 1 )
		{
			options = { 
				quality : 75, 
				destinationType : Camera.DestinationType.FILE_URI, 
				sourceType : Camera.PictureSourceType.CAMERA, 
				allowEdit : true,
				encodingType: Camera.EncodingType.JPEG,
				targetWidth: 600,
				targetHeight: 600,
				popoverOptions: CameraPopoverOptions,
				saveToPhotoAlbum: false
			};
		}
		else
		{
			options = { 
				quality : 75, 
				destinationType : Camera.DestinationType.FILE_URI, 
				sourceType : Camera.PictureSourceType.PHOTOLIBRARY, 
				allowEdit : true,
				encodingType: Camera.EncodingType.JPEG,
				targetWidth: 600,
				targetHeight: 600,
				popoverOptions: CameraPopoverOptions,
				saveToPhotoAlbum: false
			};
		}
	 
        $cordovaCamera.getPicture(options).then(function(imageData) 
		{
			//if(index == 1 )
			//$scope.imgURI = "data:image/jpeg;base64," + imageData;
			//else
			$scope.imgURI = imageData
			var myImg = $scope.imgURI;
			var options = new FileUploadOptions();
			options.mimeType = 'jpeg';
			options.fileKey = "fileUPP";
			options.fileName = $scope.imgURI.substr($scope.imgURI.lastIndexOf('/') + 1);
			//alert(options.fileName)
			var params = {};
			//params.user_token = localStorage.getItem('auth_token');
			//params.user_email = localStorage.getItem('email');
			options.params = params;
			var ft = new FileTransfer();
			ft.upload($scope.imgURI, encodeURI($rootScope.host+'/UploadImg.php'), $scope.onUploadSuccess, $scope.onUploadFail, options);
    	});
		
		$scope.onUploadSuccess = function(data)
		{
			//data = JSON.stringify(data);
			//alert (data);
			//alert(data.response)
		$timeout(function() 
		{
			if (data.response) 
			{
				if (picturenumber == 1)
				{
				$scope.supplierimage = data.response;
				$scope.supplier_image = data.response;					
				}
				else if (picturenumber == 2)
				{
				$scope.supplierimage2 = data.response;
				$scope.supplier_image2 =data.response;					
				}
			}
		}, 300);
		}
		
		$scope.onUploadFail = function(data)
		{
			//alert("onUploadFail : " + data)
		}
    }

	
	$scope.supplier_fields  = 
	{
		"catagory" : "",
		"shopno" : "",
		"street" : "",
		"floor" : "",
		"gateno" : "",
		"market" : "",
		"phone" : "",
		"mail" : "",
		"qq" : "",
		"image" : ""
	}
	
	
	$scope.scancode = function()
	{
		
      cordova.plugins.barcodeScanner.scan(
      function (result) 
	  {
		  if (result.text)
		  {
			  
			send_data = 
		{	
			'id' : parseInt(result.text)
		};
		
		$http.post($rootScope.host+'/get_supplier_qr.php', send_data)
		.success(function(data, status, headers, config)
			{
				
			if (data.response.status == 0)
			{				
				$ionicPopup.alert({
				title: "qr code not found please try again",
				buttons: [{
					text: 'OK',
					type: 'button-positive',
				  }]
			   });	
			}
			else
			{
				$scope.supplier_fields = data.response;
				if (data.response.image)
				{
					$scope.supplier_image =data.response.image;
				}
				if (data.response.image2)
				{
					$scope.supplier_image2 =data.response.image2;
				}	
			}
			})
			.error(function(data, status, headers, config)
			{

			});	

			
			  //
		  }
		  /*
          alert("We got a barcode\n" +
                "Result: " + result.text + "\n" +
                "Format: " + result.format + "\n" +
                "Cancelled: " + result.cancelled);
		*/
      }, 
      function (error) {
          alert("Scanning failed: " + error);
      }
   );
   

	}



	
	$scope.getSuppliers = function() 
	{
		supplier_params = 
		{	
			'shopno' : $scope.fields.booth,
			'userid' : $localStorage.loggedinuserid
		};
		
		//alert ($scope.fields.booth);
		
		$http.post($rootScope.host+'/Get_Supplier.php', supplier_params)
		.success(function(data, status, headers, config)
			{
				if (data.response.id)
				{
					
					$scope.supplier_id = data.response.id;
					$scope.supplier_fields.catagory = data.response.catagory;
					$scope.supplier_fields.shopno = data.response.shopno;
					$scope.supplier_fields.street = data.response.street;
					$scope.supplier_fields.floor = data.response.floor;
					$scope.supplier_fields.gateno = data.response.gateno;
					$scope.supplier_fields.market = data.response.market;
					$scope.supplier_fields.phone = data.response.phone;
					$scope.supplier_fields.mail = data.response.mail;
					$scope.supplier_fields.qq = data.response.qq;
					
					if (data.response.image)
					{
						$scope.supplier_image = data.response.image;
						$scope.oldsupplier_image = data.response.image;
					}
					else
					{
						$scope.supplier_image = $scope.DeafultUrl;
						$scope.oldsupplier_image = '';
					}

					if (data.response.image2)
					{
						$scope.supplier_image2 = data.response.image;
						$scope.oldsupplier_image2 = data.response.image;
					}
					else
					{
						$scope.supplier_image2 = $scope.DeafultUrl;
						$scope.oldsupplier_image2 = '';
					}
					
				}
				else 
				{
					$scope.supplier_id = '';
					$scope.supplier_fields.catagory = '';
					$scope.supplier_fields.shopno = $scope.fields.booth,
					$scope.supplier_fields.street = '';
					$scope.supplier_fields.floor = '';
					$scope.supplier_fields.gateno = '';
					$scope.supplier_fields.market = '';
					$scope.supplier_fields.phone = '';
					$scope.supplier_fields.mail = '';
					$scope.supplier_fields.qq = '';
					$scope.supplier_image = $scope.DeafultUrl;
					$scope.supplier_image2 = $scope.DeafultUrl;
				}
				
			})
			.error(function(data, status, headers, config)
			{

			});	
	}


			if (BoothNumber) 
			{
			$scope.getSuppliers();			
			}
			else 
			{
			
			}

	}		
	
	
	$scope.ChangeShopNo = function(value)
	{
	if (value)
	{
	$scope.fields.booth = value;
	$scope.getSuppliers();
	}
	else 
	{
		$scope.supplier_id = '';
		$scope.supplier_fields.catagory = '';
		$scope.supplier_fields.shopno = '';
		$scope.supplier_fields.street = '';
		$scope.supplier_fields.floor = '';
		$scope.supplier_fields.gateno = '';
		$scope.supplier_fields.market = '';
		$scope.supplier_fields.phone = '';
		$scope.supplier_fields.mail = '';
		$scope.supplier_fields.qq = '';
		$scope.supplier_image = $scope.DeafultUrl;	
	}

	}
	
	$scope.clearBooth = function()
	{
		$scope.fields.booth = '';
		$scope.supplier_fields.catagory = '';
		$scope.supplier_fields.shopno = '';
		$scope.supplier_fields.street = '';
		$scope.supplier_fields.floor = '';
		$scope.supplier_fields.gateno = '';
		$scope.supplier_fields.market = '';
		$scope.supplier_fields.phone = '';
		$scope.supplier_fields.mail = '';
		$scope.supplier_fields.qq = '';
		$scope.supplier_fields.image = '';
	}
	
	$scope.saveSupplier = function() 
	{ 
		if (!$scope.supplier_fields.shopno)
		{
			$ionicPopup.alert({
			title: "please input shop number",
			buttons: [{
				text: 'OK',
				type: 'button-positive',
			  }]
		   });	
		   
		}
		else 
		{
		if ($scope.supplier_id)
		{
			$scope.editsupplier = true;
			$scope.supplierUrl = 'update_supplier.php';
		}
		else 
		{
			$scope.editsupplier = false;
			$scope.supplierUrl = 'add_new_supplier.php';
		}
		
		$scope.newsupplierimage = ($scope.supplierimage !="" ? $scope.supplierimage : $scope.oldsupplier_image);
		$scope.newsupplierimage2 = ($scope.supplierimage2 !="" ? $scope.supplierimage2 : $scope.oldsupplier_image2);
	
	
		
			save_supplier_data = 
		{		
			'id' : $scope.supplier_id,
			'userid' : $localStorage.loggedinuserid,
			'quote_id' : $scope.catagoryid,
			'catagory' : $scope.supplier_fields.catagory,
			'shopno' : $scope.supplier_fields.shopno,
			'street' : $scope.supplier_fields.street,
			'floor' : $scope.supplier_fields.floor,
			'gateno' : $scope.supplier_fields.gateno,
			'market' : $scope.supplier_fields.market,
			'phone' : $scope.supplier_fields.phone,
			'mail' : $scope.supplier_fields.mail,
			'qq' : $scope.supplier_fields.qq,
			'image' : $scope.newsupplierimage,
			'image2' : $scope.newsupplierimage2
		};
		
		//console.log(save_supplier_data);
		
		
		$http.post($rootScope.host+'/'+$scope.supplierUrl, save_supplier_data)
		.success(function(data, status, headers, config)
			{

			})
			.error(function(data, status, headers, config)
			{

			});

				if ($scope.supplier_fields.shopno)
				{
					$scope.blankbooth  = '';
				}
				
				$scope.fields.booth = $scope.supplier_fields.shopno;
				$scope.TotalBooth++;
				$scope.closeBoothModal();
				
	}
	}
 
 $scope.closeBoothModal = function() 
 {
	 $scope.suppliersModal.hide();
 }
// });
 
 
 
 $scope.changeToggles = function() 
 {
	// alertbox = confirm("you are about to leave the product page , any unsaved changes will not be saved")
	// if (alertbox)
	 //{
		 $rootScope.productIndex = $scope.params.currentpage;
		 //alert ($rootScope.productIndex)
		 $rootScope.savedFields = $scope.fields;
		 window.location.href = "#/app/catagory/"+$scope.catagoryid+"/1";
	// }
	 
 }
 
 	 $ionicModal.fromTemplateUrl('image-modal.html', {
      scope: $scope,
      animation: 'slide-in-up'
    }).then(function(imageModal) {
      $scope.imageModal = imageModal;
    });
	
	$scope.closeImageModal = function()
	{
		$scope.imageModal.hide();
	}
 
	$scope.goToSlide = function(index)
	{
		if (index ==1 && $scope.firstImage == $scope.DeafultUrl)
		{
			$scope.CameraOption(1);
		}
		else if(index ==1)
		{
			$scope.imageModal.show();
		}
		
		
		if (index ==2 && $scope.secondImage == $scope.DeafultUrl)
		{
			$scope.CameraOption(1);
		}
		else if(index ==2)
		{
			$scope.imageModal.show();
		}
		
		
		if (index ==3 && $scope.thirdImage == $scope.DeafultUrl)
		{
			$scope.CameraOption(1);
		}
		else if(index ==3)
		{
			$scope.imageModal.show();
		}		
	
		
		//$ionicSlideBoxDelegate.slide(index);
		//alert (index)
	}

	$ionicPlatform.registerBackButtonAction(function (event) 
	{	
	   var confirmPopup = $ionicPopup.confirm({
		 title: "do you want to close the app?",
		 template: ''
	   });

	   confirmPopup.then(function(res) {
		if(res) 
		{
			navigator.app.exitApp();
		}
		else
		{
			event.preventDefault();
		}

	   });
    }
	,100);
	
})	




.controller('quotesCtrl', function($scope,$http,$rootScope,$stateParams,$localStorage,$ionicModal,$ionicPlatform,$ionicPopup) {
	
	//alert ($rootScope.custumerid);
	$scope.$on('$ionicView.enter', function(e) {
	 
	 
	$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';

	var myDate = new Date();
	var month = myDate.getMonth() + 1;
	var day = myDate.getDate();
	
	if(day<10) {
		day='0'+day
	} 
	if(month<10) {
		month='0'+month
	} 
	var prettyDate =(day +'-'+ month) +'-'+ myDate.getFullYear().toString().substr(2,2);

	$scope.todayquotes = 0;
	$scope.BackButtonText = 'Back';
	
	$scope.myGoBack = function()
	{
		//alert ($localStorage.catagoryid);
		if ($scope.todayquotes == 0)
		{
			window.history.back();
		}
		else 
		{
			window.location.href = "#/app/addproduct/";
		}
		
	}
 
 
		$scope.showcatagories = function() 
		{ 
	
			catagory_params = 
			{	
			'user' : $localStorage.loggedinuserid,
			'custumer' : $rootScope.custumerid
			};
			
        $http.post($rootScope.host+'/catagories.php',catagory_params)
		.success(function(data, status, headers, config)
			{
				$scope.quotes = data.response;	
				console.log("Quotes : " , $scope.quotes)
				for (i = 0; i < data.response.length; i++)
				{
					if ($scope.quotes[i].pubdate == prettyDate)
					{
						$scope.todayquotes  = 1;
						$scope.BackButtonText = 'Keep working';
						//$localStorage.catagoryid = $scope.quotes[i].id;
					}
				}
				
			})
			.error(function(data, status, headers, config)
			{

			});	

		}
		
		$scope.showcatagories();
	$scope.cutTitle = function(title,num)
	{
		$scope.datetitle = 	title.substring(0,8);
		$scope.infotitle =  title.substring(8);
		
		if(num ==0)
		return $scope.datetitle
		else
		return $scope.infotitle
	}
	
	$scope.deleteCat = function(id,title) 
	{ 
	
	   var confirmPopup = $ionicPopup.confirm({
		 title: 'confirm deletion of quote '+title+'?',
		 template: ''
	   });

	   confirmPopup.then(function(res) {
		if(res) 
		{
			$http.get($rootScope.host+'/del_catagory.php?user='+$localStorage.loggedinuserid+"&id="+id)
				.success(function(data, status, headers, config)
			{
				$scope.showcatagories();
			})
			.error(function(data, status, headers, config)
			{

			});	
		} 

	   });	
	}
	
	$scope.editCat = function(id) 
	{
		window.location.href = "#/app/editquote/"+id;
	}
	
	$scope.CatProducts = function(id,title) 
	{
		/*
			//products modal.		
		  $ionicModal.fromTemplateUrl('templates/products_modal.html', {
			scope: $scope
		  }).then(function(modal) {
			$scope.modal = modal;
			$scope.modal.show();
		  });
	*/

	
		$scope.split = title.split(" ");
		$scope.titlesplit = title.substr(title.indexOf(" ") + 1);
		$localStorage.quotedate = $scope.split[0];
		$localStorage.quotename   = $scope.titlesplit;
		$localStorage.catagoryid = id;
		window.location.href = "#/app/products/";
		//window.location.href = "#/app/addproduct/";
	}
	
	
	$scope.sendCsv = function(index)
	{
	   var confirmPopup = $ionicPopup.confirm({
		 title: 'CSV of the products will be sent to '+$localStorage.loggedinuseremail+'?',
		 template: ''
	   });

	   confirmPopup.then(function(res) {
		if(res) 
		{
         $http.get($rootScope.host+'/products_export.php?user='+$localStorage.loggedinuserid+'&catagory='+index)
		.success(function(data, status, headers, config)
			{	
				$ionicPopup.alert({
				title: "products have been sent as excel to your email.",
				buttons: [{
					text: 'OK',
					type: 'button-positive',
				  }]
			   });	
			})
			.error(function(data, status, headers, config)
			{

			});		
		} 
	   });			
	}
	
	$ionicPlatform.registerBackButtonAction(function (event) 
	{
		if ($scope.todayquotes =="1")
		{
			window.location.href = "#/app/addproduct/";
		}
		else 
		{
		window.location.href = "#/app/welcome/";	
		}
		
    }
	,100);	
	
});			
})

.controller('quotesEditCtrl', function($scope,$http,$rootScope,$stateParams,$localStorage,$ionicModal) {

	$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';

			catagories_params = 
		{	
			'user' : $localStorage.loggedinuserid,
			'id' :   $stateParams.id
		};
			
			$http.post($rootScope.host+'/catagories.php',catagories_params)
				.success(function(data, status, headers, config)
			{
				$scope.quotename = data.response.title;
				
			})
			.error(function(data, status, headers, config)
			{

			});	
		
		
		$scope.updateQuoteBtn = function() 
		{
			
			quotes_params = 
			{	
				'user' : $localStorage.loggedinuserid,
				'title' : $scope.quotename
			};
			
			$http.post($rootScope.host+'/update_catagory.php?user='+$localStorage.loggedinuserid+"&id="+$stateParams.id,quotes_params)
				.success(function(data, status, headers, config)
			{
				window.location.href = "#/app/quotes";
			})
			.error(function(data, status, headers, config)
			{

			});	
		}
		
		$scope.editCatagory  = function() 
		{ 
		window.location.href = "#/app/catagory/"+$stateParams.id+"/0";
		}
		$scope.editMarket = function() 
		{ 
		window.location.href = "#/app/markets/"+$stateParams.id;
		}
})


.controller('customersCtrl', function($scope,$http,$rootScope,$stateParams,$localStorage,$ionicModal,$ionicPlatform,$ionicPopup) {
	
//		$scope.$on('$ionicView.enter', function(e) {
	
		$scope.imagePath = $rootScope.host;
		$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
		$scope.custumerClicked = 0;
		
		$scope.setCustumer = function()
		{
			if ($localStorage.custumerid)
			{
				$rootScope.defaultCustumerIndex = $localStorage.custumerid  
				$rootScope.defaultCustumer =  $localStorage.custumername;	
			}
			else
			{
				$localStorage.custumerid = $rootScope.defaultCustumerIndex;
				$localStorage.custumername = $rootScope.defaultCustumer;	
			}			
		}
		
		$scope.setCustumer();
		

		
		$scope.getCustumers = function() 
		{ 
			
			customers_params = 
			{	
				'user' : $localStorage.loggedinuserid,
			};
			
			$http.post($rootScope.host+'/customers.php',customers_params)
				.success(function(data, status, headers, config)
			{
								
				$scope.customers = data.response;

				for (i = 0; i < $scope.customers.length; i++)
				{
					if ($scope.customers[i].id == $localStorage.custumerid)
					{
						$scope.customers[i].isClicked = 1;
					}
					else
					{
						$scope.customers[i].isClicked = 0;
					}
					
				}

				
			})
			.error(function(data, status, headers, config)
			{

			});	
			

		}
		
		$scope.getCustumers();
		
		$scope.editCustomer = function(id) 
		{ 
			window.location.href = "#/app/custumermanage/"+id+"/";
		}

			
	$scope.deleteCustomer = function(id,title)  
	{
		
	   var confirmPopup = $ionicPopup.confirm({
		 title: 'confirm deletion of customer '+title+'?',
		 template: ''
	   });

	   confirmPopup.then(function(res) {
		if(res) 
		{
			del_params = 
			{	
				'id' : id,
				'user' : $localStorage.loggedinuserid
			};
			
			$http.post($rootScope.host+'/del_customer.php', del_params)
			.success(function(data, status, headers, config)
			{
				$scope.getCustumers();
			})
			.error(function(data, status, headers, config)
			{
			});			
		} 
	   });
	}	

	$scope.addCustumer = function() 
	{
		window.location.href = "#/app/custumermanage/0/";
	}
	
	$scope.selectCustumer = function(id,title,system,index) 
	{
		
		for (i = 0; i < $scope.customers.length; i++)
		{
			$scope.customers[i].isClicked = 0;
		}	


		 if ($scope.customers[index].isClicked == 1)
		{
			$scope.customers[index].isClicked = 0;
		}
		else
		{
			$scope.customers[index].isClicked = 1;
		}

		
		
	if (system == 1)
	{
		$rootScope.custumerid = 0;
		$localStorage.custumerid = 0;
		$rootScope.defaultCustumerIndex = '0';
	}
	else 
	{
		$rootScope.custumerid = id;
		$localStorage.custumerid = id;
	}
		$rootScope.isSettings = true;
		$localStorage.custumername = title;
		$rootScope.defaultCustumer = title;
		window.location.href = "#/app/welcome/";
	}
	
//});	

	$ionicPlatform.registerBackButtonAction(function (event) 
	{
		window.location.href = "#/app/welcome/";
    }
	,100);



})

.controller('ForgotPasswordCtrl', function($scope,$http,$rootScope,$stateParams,$localStorage,$ionicModal,$ionicPopup) {

$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';

$scope.sendPassword = function()
{
		if (!$scope.email)
		{
			$ionicPopup.alert({
			title: "please input your email",
			buttons: [{
				text: 'OK',
				type: 'button-positive',
			  }]
		   });				
		}
		else
		{
			forgot_pass_params = 
			{
				'email' : $scope.email
			};
			
			$http.post($rootScope.host+'/forgot_password.php',forgot_pass_params)
				.success(function(data, status, headers, config)
			{
				if (data.response.status == 1)
				{
					$ionicPopup.alert({
					title: "Your password was sent to your email",
					buttons: [{
						text: 'OK',
						type: 'button-positive',
					  }]
				   });	

		   
					$scope.email = '';
				}
				else
				{
					$ionicPopup.alert({
					title: "email doesnt exist please try again",
					buttons: [{
						text: 'OK',
						type: 'button-positive',
					  }]
				   });
				   
					$scope.email = '';
				}
			})
			.error(function(data, status, headers, config)
			{

			});				
		}

	
}

})




.controller('customermanageCtrl', function($scope,$http,$cordovaCamera,$ionicActionSheet,$rootScope,$stateParams,$localStorage,$ionicModal,$timeout,$ionicPopup) {
	
	$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
	$scope.imagePath = $rootScope.host; 
	$scope.defaultImage = "img/main/addImg.png";
	$scope.showsubmit = false;
	$scope.editableDefault = false;
	
	$scope.custumerfields = 
	{
		"customername" : "",
		"company" : "",
		"email" : "",
		"mobile" : "",
		"products" : "" 
	}
	
	
	if ($stateParams.id != 0)
	{
	customers_params = 
	{
		'id' : $stateParams.id,
		'user' : $localStorage.loggedinuserid
	};
	
	$http.post($rootScope.host+'/customers.php',customers_params)
		.success(function(data, status, headers, config)
	{
		$scope.custumerfields.customername = data.response.name;
		$scope.custumerfields.company = data.response.company;
		$scope.custumerfields.email = data.response.email;
		$scope.custumerfields.mobile = data.response.mobile;
		$scope.custumerfields.products = data.response.products;
			
		//if (data.response.image) {
		$scope.image = data.response.image;	
		$scope.custumerimage = data.response.image;	
		
		if (data.response.image =="")
		{
			$scope.defaultImage = $scope.defaultImage;
		}
		else
		{
			$scope.defaultImage = $rootScope.host+$scope.image;
		}
		
		//}
		//else {
		//	$scope.image = "img/main/addimage.jpg";
		//}
		$scope.image2 = data.response.image2;
		if (data.response.system == 1)
		{
			$scope.editableDefault = true;
		}
	})
	.error(function(data, status, headers, config)
	{

	});	
	}
	$scope.custList = function() { 
	window.location.href = "#/app/customers/";
	}


	$scope.CameraOption = function() 
	{
	   // Show the action sheet
	   var hideSheet = $ionicActionSheet.show({
		 buttons: [
		   { text: 'Open Image Gallery' },
		   { text: 'Open Camera' },
		   { text: 'Back'}
		 ],
		 cancelText: 'Cancel',
		 cssClass: 'social-actionsheet',
		 cancel: function() {
			  return true;
			},
		 buttonClicked: function(index) {
			 if(index<2)
			 	$scope.takePicture(index);
			 
		   return true;
		 }});
			 // For example's sake, hide the sheet after two seconds
		   $timeout(function() {
			 hideSheet();
		   }, 2000);
		
	};

	// destinationType : Camera.DestinationType.DATA_URL,  CAMERA
	$scope.takePicture = function(index) {
		 var options ;
		 
		if(index == 1 )
		{
			options = { 
				quality : 75, 
				destinationType : Camera.DestinationType.FILE_URI, 
				sourceType : Camera.PictureSourceType.CAMERA, 
				allowEdit : true,
				encodingType: Camera.EncodingType.JPEG,
				targetWidth: 600,
				targetHeight: 600,
				popoverOptions: CameraPopoverOptions,
				saveToPhotoAlbum: false
			};
		}
		else
		{
			options = { 
				quality : 75, 
				destinationType : Camera.DestinationType.FILE_URI, 
				sourceType : Camera.PictureSourceType.PHOTOLIBRARY, 
				allowEdit : true,
				encodingType: Camera.EncodingType.JPEG,
				targetWidth: 600,
				targetHeight: 600,
				popoverOptions: CameraPopoverOptions,
				saveToPhotoAlbum: false
			};
		}
	 
        $cordovaCamera.getPicture(options).then(function(imageData) 
		{
			//if(index == 1 )
			//$scope.imgURI = "data:image/jpeg;base64," + imageData;
			//else
			$scope.imgURI = imageData
			var myImg = $scope.imgURI;
			var options = new FileUploadOptions();
			options.mimeType = 'jpeg';
			options.fileKey = "fileUPP";
			options.fileName = $scope.imgURI.substr($scope.imgURI.lastIndexOf('/') + 1);
			//alert(options.fileName)
			var params = {};
			//params.user_token = localStorage.getItem('auth_token');
			//params.user_email = localStorage.getItem('email');
			options.params = params;
			var ft = new FileTransfer();
			ft.upload($scope.imgURI, encodeURI($rootScope.host+'/UploadImg.php'), $scope.onUploadSuccess, $scope.onUploadFail, options);
    	});
		
		$scope.onUploadSuccess = function(data)
		{
		$timeout(function() 
		{	
			//data = JSON.stringify(data);
			//alert (data);
			//alert(data.response)
			if (data.response) 
			{
				$scope.newimage = data.response;
				$scope.defaultImage = $rootScope.host+data.response;
				//alert ($scope.defaultImage )
				//alert ($scope.defaultImage);
				//$scope.showsubmit = false;
				//$scope.uploadedimage = $rootScope.host+data.response.imagepath;
			}
		}, 300);	
		}
		
		$scope.onUploadFail = function(data)
		{
			//alert("onUploadFail : " + data)
		}
    }
	
	
	$scope.saveCust = function() 
	{
	
	$rootScope.isSettings = true;
	
	
	if  ($stateParams.id =="0") 
	{
		$scope.edit = false;
		$scope.custurl = 'add_new_custumer.php';
	}		
	else 
	{
		$scope.edit = true;
		$scope.custurl = 'update_custumer.php';
	}

	if ($scope.newimage) 
	{
		$scope.custumerimage = $scope.newimage;
	}
	else 
	{
		$scope.custumerimage = $scope.custumerimage;
	}

		customer_params = 
		{
			'id' : $stateParams.id,
			'user' : $localStorage.loggedinuserid,
			'catagory' : $localStorage.catagoryid, 
			'customername' : $scope.custumerfields.customername,
			'company' : $scope.custumerfields.company,
			'email' : $scope.custumerfields.email,
			'mobile' : $scope.custumerfields.mobile,
			'products' : $scope.custumerfields.products,
			'image' : $scope.custumerimage,
			'image2' : $scope.image2
		};

		if ($scope.custumerfields.customername =="")
		{
			$ionicPopup.alert({
			 title: 'please fill in customer name',
			buttons: [{
				text: 'OK',
				type: 'button-positive',
			  }]
		   });		   
		}
		else if ($scope.custumerfields.company =="")
		{
			$ionicPopup.alert({
			 title: 'please fill in company name',
			buttons: [{
				text: 'OK',
				type: 'button-positive',
			  }]
		   });					
		}
		else if ($scope.custumerfields.email =="")
		{
			$ionicPopup.alert({
			 title: 'please fill in email address',
			buttons: [{
				text: 'OK',
				type: 'button-positive',
			  }]
		   });					
		}
		else if ($scope.custumerfields.mobile =="")
		{
			$ionicPopup.alert({
			 title: 'please fill in mobile number',
			buttons: [{
				text: 'OK',
				type: 'button-positive',
			  }]
		   });					
		}
		else if ($scope.custumerfields.products =="")
		{
			$ionicPopup.alert({
			title: 'please fill in products type',
			buttons: [{
				text: 'OK',
				type: 'button-positive',
			  }]
		   });					
		}
		else 
		{		
			$http.post($rootScope.host+'/'+$scope.custurl,customer_params)
				.success(function(data, status, headers, config)
			{
			})
			.error(function(data, status, headers, config)
			{
			});	
			window.location.href = "#/app/customers/";
		}
		
	}
	
})


.controller('catagoryCtrl', function($scope,$http,$rootScope,$stateParams,$localStorage,$ionicModal) {
  
  	$scope.navTitle='<p class="ToogleHeaderTitle">Setting</p>'
	$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
	

	if ($stateParams.id) 
	{
		$scope.id = $stateParams.id;
	}
	else 
	{
		$scope.id = $localStorage.catagoryid;
	}
	
	
	$scope.toggle_params = 
	{
		'picture' : 0,
		'itemno' : 0,
		'price' : 0,
		'qty' : 0,
		'cbm' : 0,
		'weight' : 0,
		'supno' : 0,
		'remarks' : 0,
		'desc' : 0,
		'size' : 0,
		'material' : 0,
		'stock' : 0,
		'custom_toggle' : 0,
		'customText1' : '',
		'custom_toggle2' :0,
		'customText2' : '',
		'custom_toggle3' : 0,
		'customText3' : ''
	};
	
	$scope.changeToggle = function(id)
	{
		switch (id) {
			case 1:
				$scope.toggle_params.picture = ($scope.toggle_params.picture ? 0 : 1);
				break;
			case 2:
				$scope.toggle_params.itemno = ($scope.toggle_params.itemno ? 0 : 1);
				break;
			case 3:
				$scope.toggle_params.price = ($scope.toggle_params.price ? 0 : 1);
				break;
			case 4:
				$scope.toggle_params.qty = ($scope.toggle_params.qty ? 0 : 1);
				break;
			case 5:
				$scope.toggle_params.cbm = ($scope.toggle_params.cbm ? 0 : 1);
				break;
			case 6:
				$scope.toggle_params.weight = ($scope.toggle_params.weight ? 0 : 1);
				break;
			case 7:
				$scope.toggle_params.supno = ($scope.toggle_params.supno ? 0 : 1);
				break;
			case 8:
				$scope.toggle_params.remarks = ($scope.toggle_params.remarks ? 0 : 1);
				break;
			case 9:
				$scope.toggle_params.desc = ($scope.toggle_params.desc ? 0 : 1);
				break;
			case 10:
				$scope.toggle_params.size = ($scope.toggle_params.size ? 0 : 1);
				break;
			case 11:
				$scope.toggle_params.material = ($scope.toggle_params.material ? 0 : 1);
				break;
			case 12:
				$scope.toggle_params.stock = ($scope.toggle_params.stock ? 0 : 1);
				break;	
			case 13:
				$scope.toggle_params.custom_toggle = ($scope.toggle_params.custom_toggle ? 0 : 1);
				break;	
			case 14:
				$scope.toggle_params.custom_toggle2 = ($scope.toggle_params.custom_toggle2 ? 0 : 1);
				break;	
			case 15:
				$scope.toggle_params.custom_toggle3 = ($scope.toggle_params.custom_toggle3 ? 0 : 1);
				break;					
		}
	}
	
	
	$scope.getInfo = function ()
	{
		data_params = 
		{
			'id' : $scope.id,
			'user' : $localStorage.loggedinuserid	
		}
	
		$http.post($rootScope.host+'/toggles.php',data_params)
			.success(function(data, status, headers, config)
		{
			$scope.toggle_params.picture = (data.response.picture =="1" ? true : false);
			$scope.toggle_params.itemno = (data.response.itemno =="1" ? true : false);
			$scope.toggle_params.price = (data.response.price =="1" ? true : false);
			$scope.toggle_params.qty = (data.response.qty =="1" ? true : false);
			$scope.toggle_params.cbm = (data.response.cbm =="1" ? true : false);
			$scope.toggle_params.weight = (data.response.weight =="1" ? true : false);
			$scope.toggle_params.supno = (data.response.supno =="1" ? true : false);
			$scope.toggle_params.remarks = (data.response.remarks =="1" ? true : false);
			$scope.toggle_params.desc = (data.response.description =="1" ? true : false);
			$scope.toggle_params.size = (data.response.size =="1" ? true : false);
			$scope.toggle_params.material = (data.response.material =="1" ? true : false);
			$scope.toggle_params.stock = (data.response.stock =="1" ? true : false);
			$scope.toggle_params.custom_toggle = (data.response.custom_toggle =="1" ? true : false);
			$scope.toggle_params.customText1 = data.response.custom_field1;
			$scope.toggle_params.custom_toggle2 = (data.response.custom_toggle2 == "1" ? true : false);
			$scope.toggle_params.customText2 = data.response.custom_field2;
			$scope.toggle_params.custom_toggle3 = (data.response.custom_toggle3 =="1" ? true : false);
			$scope.toggle_params.customText3 = data.response.custom_field3;	
		})
		.error(function(data, status, headers, config)
		{
		});	
	}
	
	$scope.getInfo();

	
	$scope.saveChanges = function() 
	{	
		$scope.saveParams = 
	{
		'id' : $scope.id,
		'user' : $localStorage.loggedinuserid,
		'picture' : $scope.toggle_params.picture,
		'itemno' : $scope.toggle_params.itemno,
		'price' : $scope.toggle_params.price,
		'qty' : $scope.toggle_params.qty,
		'cbm' : $scope.toggle_params.cbm,
		'weight' : $scope.toggle_params.weight,
		'supno' : $scope.toggle_params.supno,
		'remarks' : $scope.toggle_params.remarks,
		'desc' : $scope.toggle_params.desc,
		'size' : $scope.toggle_params.size,
		'material' : $scope.toggle_params.material,
		'stock' : $scope.toggle_params.stock,
		'custom_toggle' : $scope.toggle_params.custom_toggle,
		'custom_field1' : $scope.toggle_params.customText1,
		'custom_toggle2' : $scope.toggle_params.custom_toggle2,
		'custom_field2' : $scope.toggle_params.customText2,
		'custom_toggle3' : $scope.toggle_params.custom_toggle3,
		'custom_field3' : $scope.toggle_params.customText3
	};	
			
		$http.post($rootScope.host+'/save_toggles.php',$scope.saveParams)
			.success(function(data, status, headers, config)
		{	
			if ($stateParams.id) {
			if ($stateParams.itemId =="0")
			{
				window.location.href = "#/app/quotes";
			}
			else 
			{
				window.location.href = "#/app/addproduct/"+$rootScope.productIndex;
			}
			
			}
			else {
				window.location.href = "#/app/welcome/";
			}
		})
		.error(function(data, status, headers, config)
		{
		});	
	}
	
	$scope.BackSaveChanges = function()
	{
		$scope.saveChanges();
	}
	
		
})

.controller('SupplierCtrl', function($scope,$http,$rootScope,$stateParams,$localStorage,$ionicModal,$cordovaCamera,$timeout,$ionicPopup) {
  
	$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
	
	$scope.supplier_image = $scope.DeafultUrl;
	$scope.supplier_image2 = $scope.DeafultUrl;
	$scope.supplierid = '';
		
	$scope.supplier_fields = 
	{
		"catagory": "",
		"shopno": "",
		"street": "",
		"floor": "",
		"gateno": "",
		"market": "",
		"phone": "",
		"mail": "",
		"qq": ""	
	}	
	

$scope.getSupplierData = function()
{
	get_params = 
	{	
	'user' : $localStorage.loggedinuserid
	};
	//console.log(register_params)
   $http.post($rootScope.host+'/get_supplier_data.php', get_params)
	.success(function(data, status, headers, config)
		{
			
			if (data.response.id)
			{
				
				$scope.edit = true;
				$scope.supplierid = data.response.id;
				$scope.supplier_fields = data.response;
				if (data.response.image)
				{
					$scope.supplier_image = data.response.image;
				}
				if (data.response.image2)
				{
				 $scope.supplier_image2 = data.response.image2;	
				}	
			}
			else
			{
				$scope.edit = false;
			}
		})
		.error(function(data, status, headers, config)
		{
		});					
}	

$scope.getSupplierData();
	
$scope.takeSupplierPicture = function(index,picturenumber) 
{
	var options ;
	if(index == 1 )
	{
		options = { 
			quality : 75, 
			destinationType : Camera.DestinationType.FILE_URI, 
			sourceType : Camera.PictureSourceType.CAMERA, 
			allowEdit : true,
			encodingType: Camera.EncodingType.JPEG,
			targetWidth: 600,
			targetHeight: 600,
			popoverOptions: CameraPopoverOptions,
			saveToPhotoAlbum: false
		};
	}
	else
	{
		options = { 
			quality : 75, 
			destinationType : Camera.DestinationType.FILE_URI, 
			sourceType : Camera.PictureSourceType.PHOTOLIBRARY, 
			allowEdit : true,
			encodingType: Camera.EncodingType.JPEG,
			targetWidth: 600,
			targetHeight: 600,
			popoverOptions: CameraPopoverOptions,
			saveToPhotoAlbum: false
		};
	}
 
	$cordovaCamera.getPicture(options).then(function(imageData) 
	{
		//if(index == 1 )
		//$scope.imgURI = "data:image/jpeg;base64," + imageData;
		//else
		$scope.imgURI = imageData
		var myImg = $scope.imgURI;
		var options = new FileUploadOptions();
		options.mimeType = 'jpeg';
		options.fileKey = "fileUPP";
		options.fileName = $scope.imgURI.substr($scope.imgURI.lastIndexOf('/') + 1);
		//alert(options.fileName)
		var params = {};
		//params.user_token = localStorage.getItem('auth_token');
		//params.user_email = localStorage.getItem('email');
		options.params = params;
		var ft = new FileTransfer();
		ft.upload($scope.imgURI, encodeURI($rootScope.host+'/UploadImg.php'), $scope.onUploadSuccess, $scope.onUploadFail, options);
	});
	
	$scope.onUploadSuccess = function(data)
	{
		//data = JSON.stringify(data);
		//alert (data);
		//alert(data.response)
	$timeout(function() {	
		if (data.response) {
			if (picturenumber == 1)
			{
			$scope.supplierimage = data.response;
			$scope.supplier_image = data.response;					
			}
			else if (picturenumber == 2)
			{
			$scope.supplierimage2 = data.response;
			$scope.supplier_image2 = data.response;					
			}
		}
	}, 300);	
	}
	
	$scope.onUploadFail = function(data)
	{
		//alert("onUploadFail : " + data)
	}
}

	
	$scope.clearGeneralBooth = function()
	{
		$scope.supplier_fields.catagory = '';
		$scope.supplier_fields.shopno = '';
		$scope.supplier_fields.street = '';
		$scope.supplier_fields.floor = '';
		$scope.supplier_fields.gateno = '';
		$scope.supplier_fields.market = '';
		$scope.supplier_fields.phone = '';
		$scope.supplier_fields.mail = '';
		$scope.supplier_fields.qq = '';
	}
	
	
	
	
	$scope.saveGeneralSupplier = function()
	{
		/*
		if ($scope.supplier_fields.catagory == "")
		{
			$ionicPopup.alert({
			title: "please fill in catagory name",
			buttons: [{
				text: 'OK',
				type: 'button-positive',
			  }]
		   });	
		   
		}
		*/
		 if ($scope.supplier_fields.shopno == "")
		{
			$ionicPopup.alert({
			title: "please fill in shop number",
			buttons: [{
				text: 'OK',
				type: 'button-positive',
			  }]
		   });				
		}
		else if ($scope.supplier_fields.street == "")
		{
			$ionicPopup.alert({
			title: "please fill in street name",
			buttons: [{
				text: 'OK',
				type: 'button-positive',
			  }]
		   });				
		}
		else if ($scope.supplier_fields.floor == "")
		{
			$ionicPopup.alert({
			title: "please fill in floor number",
			buttons: [{
				text: 'OK',
				type: 'button-positive',
			  }]
		   });				
		}
		else if ($scope.supplier_fields.gateno == "")
		{
			$ionicPopup.alert({
			title: "please fill in gate number",
			buttons: [{
				text: 'OK',
				type: 'button-positive',
			  }]
		   });				
		}
		else if ($scope.supplier_fields.market == "")
		{
			$ionicPopup.alert({
			title: "please fill in market name",
			buttons: [{
				text: 'OK',
				type: 'button-positive',
			  }]
		   });				
		}
		else if ($scope.supplier_fields.phone == "")
		{
			$ionicPopup.alert({
			title: "please fill in phone number",
			buttons: [{
				text: 'OK',
				type: 'button-positive',
			  }]
		   });				
		}
		else if ($scope.supplier_fields.mail == "")
		{
			$ionicPopup.alert({
			title: "please fill in mail",
			buttons: [{
				text: 'OK',
				type: 'button-positive',
			  }]
		   });				
		}
		else if ($scope.supplier_fields.qq == "")
		{
			$ionicPopup.alert({
			title: "please fill in qq",
			buttons: [{
				text: 'OK',
				type: 'button-positive',
			  }]
		   });				
		}

		else 
		{
			
			if ($scope.edit)
			{
				$scope.Url = 'update_general_supplier.php';
			}
			else 
			{
				$scope.Url = 'save_supplier.php';
			}
			
		$scope.newsupplierimage = ($scope.supplierimage !="" ? $scope.supplierimage : $scope.supplier_image);
		$scope.newsupplierimage2 = ($scope.supplierimage2 !="" ? $scope.supplierimage2 : $scope.supplier_image2);
			
						
		   send_params = 
			{	
				'id' : $scope.supplierid,
				'user' : $localStorage.loggedinuserid,
				'user_name' : $localStorage.loggedinusername,
				'user_email' : $localStorage.loggedinuseremail,
				'catagory' : $scope.supplier_fields.catagory,
				'shopno' : $scope.supplier_fields.shopno,
				'street' : $scope.supplier_fields.street,
				'floor' : $scope.supplier_fields.floor,
				'gateno' : $scope.supplier_fields.gateno,
				'market' : $scope.supplier_fields.market,
				'phone' : $scope.supplier_fields.phone,
				'mail' : $scope.supplier_fields.mail,
				'qq' : $scope.supplier_fields.qq,
				'image' : $scope.newsupplierimage,
				'image2' : $scope.newsupplierimage2
			};
			//console.log(register_params)
		   $http.post($rootScope.host+'/'+$scope.Url, send_params)
			.success(function(data, status, headers, config)
				{
				 if ($scope.edit)
				 {
					$ionicPopup.alert({
					title: "updated successfully",
					buttons: [{
						text: 'OK',
						type: 'button-positive',
					  }]
				   });	
		   
				 }
				 else
				 {
					$ionicPopup.alert({
					title: "saved successfully ,an email has been sent to you with your supplier qr code.",
					buttons: [{
						text: 'OK',
						type: 'button-positive',
					  }]
				   });	
				   
				 }
				 
				 window.location.href = "#/app/welcome/";
				})
				.error(function(data, status, headers, config)
				{

				});				
		}
	}
	
		
})


.controller('ProductsExcel', function($scope,$http,$rootScope,$stateParams,$localStorage,$ionicModal) {
})


.controller('LanguagesCtrl', function($scope,$http,$rootScope,$stateParams,$localStorage,$ionicModal,$translate,$state) {


	if ($localStorage.loggedinuserid !="")
	{
		//$state.go('app.main');
	}
	
	
	$scope.selectedLanguge = '';
	
	$scope.getLanguages = function()
	{
		$http.get('js/data/languages.json')
		.success(function(data, status, headers, config)
		{	
			$scope.LanguagesArray = data;
			
			for (i = 0; i < $scope.LanguagesArray.length; i++)
			{
				if ($localStorage.language == $scope.LanguagesArray[i].short)
				{
					$scope.LanguagesArray[i].selected = 1;
				}
				else
				{
					$scope.LanguagesArray[i].selected = 0;
				}
				
			}
			
		})
		.error(function(data, status, headers, config)
		{
		});		
	}
	
	$scope.getLanguages();
	
	
	$scope.selectLanguage= function(index,language)
	{
		for (i = 0; i < $scope.LanguagesArray.length; i++)
		{
			$scope.LanguagesArray[i].selected = 0;
		}
			
		 $translate.use(language);
		 $localStorage.language = language;
		 $rootScope.defaultLanguage = language;
		 $scope.selectedLanguge = language;
		 $scope.LanguagesArray[index].selected = 1;
	}
	
	$scope.saveLanguage = function()
	{
		$scope.languageSet = 0;
		
		
		for (i = 0; i < $scope.LanguagesArray.length; i++)
		{
			if ($scope.LanguagesArray[i].selected == 1)
			{
				$scope.languageSet = 1;
			}
		}
		
		if ($scope.languageSet == 0)
		{
			$ionicPopup.alert({
			title: "please select a language",
			buttons: [{
				text: 'OK',
				type: 'button-positive',
			  }]
		   });				
		}
		
		else
		{
			$localStorage.language = $scope.selectedLanguge;
			$rootScope.defaultLanguage = $scope.selectedLanguge;
			if ($localStorage.loggedinuserid)
			window.location.href = "#/app/welcome/";
			else
			window.location.href = "#/app/register";
		}

		
	}
	
		

})


.directive('ngEnter', function () {
    return function (scope, element, attrs) {
        element.bind("keydown keypress", function (event) {
			
            if (event.charCode == 13 && /Android/.test(navigator.userAgent)) {
                scope.$apply(function (){
                    scope.$eval(attrs.ngEnter);
                });

                event.preventDefault();
            }
        });
    };
})

